#!/usr/bin/python

import socket
import sys
import os
import subprocess
import commands
from thread import *

HOST = 'localhost'
PORT = 8960
SIZE = 512

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket created'

# On essaye de bind l'host et le port, si le port est deja occupe, on arrete le serveur
try:
	s.bind((HOST, PORT))
except socket.error as msg:
	print 'Bind failed. Error code : ' + str(msg[0]) + ' - ' + msg[1]
	sys.exit()

print 'Socket bind complete'

# On met le serveur sur ecoute, ici il peut accepter jusqu'a 10 connexions
s.listen(10)
print 'Socket now listening'

# On defini une fonction pour creer les thread clients
def clientThread(conn):
	conn.send('Connected to the server. You have access to a remote shell on this server. Type !close to close connection\nClient>')
	
	while True:
		cmd = conn.recv(SIZE)
		print '[' + addr[0] + ':' + str(addr[1]) + '] typed ' + cmd
		
		rcmd = cmd.replace('\r\n', '')
		
		if rcmd == '!close':
			print  'Connection to server closed for ' + addr[0] + ':' + str(addr[1])
			reply = 'Connection to server closed'
			conn.send(reply)
			conn.shutdown(socket.SHUT_RDWR)
			break;
			sys.exit(0)
			
		#elif (rcmd.split(' ')[0]=="cd") :
		#	op = commands.getoutput(rcmd.split(' ')[1])
		#	conn.send(op)
		#else:
		op = commands.getoutput(rcmd)
		conn.send(op + '\nClient>')
		
		#p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True) 
		#(output, err) = p.communicate()
		
		#reply = '[Server] OK...' + cmd
		
		if not cmd:
			break
	
	conn.close()
	
while 1:
	try:
		conn, addr = s.accept()
		print 'Connected with ' + addr[0] + ':' + str(addr[1])
		
		start_new_thread(clientThread ,(conn,))
	except:
		print 'Server closed'
		s.close()
		sys.exit()
	
s.close()
